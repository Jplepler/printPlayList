Coheed and Cambria
	<Welcome Home>


Iron Maiden
	<The Trooper (1998 Remaster)>
	<Blood Brothers>
	<Run to the Hills (1998 Remastered Version)>
	<Phantom of the Opera (1998 Remaster)>
	<Fear Of The Dark (1998 Remastered Version)>


Deep Purple
	<Smoke On The Water>
	<The Spanish Archer>
	<Bad Attitude>
	<The Unwritten Law>
	<Call Of The Wild>
	<Mad Dog>
	<Black & White>
	<Hard Lovin' Woman>
	<Strangeways>
	<Mitzi Dupree>
	<Dead Or Alive>
	<Soldier of Fortune>
	<Child in Time>
	<Black Night>
	<When A Blind Man Cries (1997 Remix)>
	<Demon's Eye>
	<Highway Star>


System of a Down
	<Toxicity>
	<B.Y.O.B.>
	<Chop Suey!>
	<Lonely Day>
	<Hypnotize>
	<Chic 'N' Stu>
	<Radio/Video>


Woodkid
	<Boat Song>
	<The Other Side>
	<Ghost Lights>
	<The Shore>
	<I Love You>
	<Stabat Mater>
	<Run Boy Run>
	<The Golden Age>
	<Conquest of Spaces>
	<Iron>
	<Shadows>
	<Where I Live>
	<The Great Escape>


Pink Floyd
	<In The Flesh (2011 Remastered Version)>
	<Comfortably Numb (2011 Remastered Version)>
	<Nobody Home (2011 Remastered Version)>
	<Goodbye Cruel World (2011 Remastered Version)>
	<Young Lust (2011 Remastered Version)>
	<The Gunner's Dream (2011 Remastered Version)>
	<Your Possible Pasts (2011 Remastered Version)>
	<The Trial (2011 Remastered Version)>
	<Don't Leave Me Now (2011 Remastered Version)>
	<In The Flesh? (2011 Remastered Version)>
	<Shine On You Crazy Diamond (Pts. 1-7)>
	<Another Brick In The Wall, Pt. 2 (2011 Remastered Version)>
	<Mother (2011 Remastered Version)>
	<Vera (2011 Remastered Version)>
	<The Happiest Days Of Our Lives (2011 Remastered Version)>
	<Goodbye Blue Sky (2011 Remastered Version)>
	<Wish You Were Here (2011 Remastered Version)>
	<High Hopes (2011 Remastered Version)>
	<The Fletcher Memorial Home (2011 Remaster)>
	<Speak To Me (2011 Remastered Version)>
	<The Post War Dream (2011 Remastered Version)>
	<Not Now John (2011 Remastered Version)>
	<Get Your Filthy Hands Off My Desert (2011 Remastered Version)>
	<One Of The Few (2011 Remastered Version)>
	<The Final Cut (2011 Remastered Version)>
	<Cluster One (2011 Remastered Version)>
	<Keep Talking>
	<Take It Back (2011 Remastered Version)>
	<What Do You Want From Me (2011 Remastered Version)>
	<A Great Day For Freedom (2011 Remastered Version)>
	<Paranoid Eyes (2011 Remastered Version)>
	<Outside The Wall (2011 Remastered Version)>
	<Echoes>
	<In the Flesh?>
	<The Thin Ice>
	<Another Brick in the Wall, Pt. 1>
	<The Happiest Days of Our Lives>
	<Another Brick in the Wall, Pt. 2>
	<Mother>
	<Goodbye Blue Sky>
	<Empty Spaces>
	<Young Lust>
	<One of My Turns>
	<Don't Leave Me Now>
	<Another Brick in the Wall, Pt. 3>
	<Goodbye Cruel World>
	<Hey You>
	<Is There Anybody Out There?>
	<Nobody Home>
	<Vera>
	<Bring the Boys Back Home>
	<Comfortably Numb>
	<The Show Must Go On>
	<In the Flesh>
	<Run Like Hell>
	<Waiting for the Worms>
	<Stop>
	<The Trial>
	<Outside the Wall>
	<Speak to Me>
	<Breathe (In the Air)>
	<On the Run>
	<Time>
	<The Great Gig in the Sky>
	<Money>
	<Us and Them>
	<Any Colour You Like>
	<Brain Damage>
	<Eclipse>
	<Astronomy Domine>
	<Lucifer Sam>
	<Matilda Mother>
	<Flaming>
	<Pow R. Toc H.>
	<Take Up Thy Stethoscope and Walk>
	<Interstellar Overdrive>
	<The Gnome>
	<Chapter 24>
	<The Scarecrow>
	<Bike>
	<Let There Be More Light>
	<Remember a Day>
	<Set the Controls for the Heart of the Sun>
	<Corporal Clegg>
	<A Saucerful of Secrets>
	<See-Saw>
	<Jugband Blues>
	<Signs of Life>
	<Learning to Fly>
	<The Dogs of War>
	<One Slip>
	<On the Turning Away>
	<Yet Another Movie>
	<Round and Around>
	<A New Machine, Pt. 1>
	<Terminal Frost>
	<A New Machine, Pt. 2>
	<Sorrow>
	<Pigs on the Wing 1>
	<Dogs>
	<Pigs (Three Different Ones)>
	<Sheep>
	<Pigs on the Wing 2>
	<Obscured by Clouds>
	<When You're In>
	<Burning Bridges>
	<The Gold It's in the...>
	<Wot's...Uh the Deal>
	<Mudmen>
	<Childhood's End>
	<Free Four>
	<Stay>
	<Absolutely Curtains>
	<Welcome to the Machine>
	<See Emily Play>
	<Have a Cigar>


Gorillaz
	<Feel Good Inc.>
	<New Genius (Brother)>
	<Punk>
	<Re-Hash>
	<O Green World>
	<Intro>
	<Rock the House>
	<5/4>
	<Slow Country>
	<November Has Come>
	<Don't Get Lost in Heaven>
	<Demon Days>
	<Dirty Harry>
	<All Alone>
	<Tomorrow Comes Today>
	<Clint Eastwood>
	<19-2000>
	<Kids with Guns>
	<Every Planet We Reach Is Dead>
	<El Ma�ana>
	<Saturnz Barz (feat. Popcaan)>
	<Andromeda (feat. DRAM)>


Lynyrd Skynyrd
	<Sweet Home Alabama>


Slayer
	<Repentless>
	<Raining Blood>
	<South Of Heaven>


Metallica
	<The Unforgiven (LP Version)>
	<The Call Of Ktulu (LP Version)>
	<Nothing Else Matters (LP Version)>
	<Enter Sandman (LP Version)>
	<The Unforgiven III>
	<The Unforgiven II (LP Version)>
	<Am I Savage?>
	<For Whom The Bell Tolls (LP Version)>
	<Sad But True (LP Version)>
	<Hardwired>
	<Atlas, Rise!>
	<Now That We�re Dead>
	<Moth Into Flame>
	<Dream No More>
	<Halo On Fire>
	<Confusion>
	<ManUNkind>
	<Here Comes Revenge>
	<Murder One>
	<Spit Out The Bone>
	<Lords Of Summer>
	<Ronnie Rising Medley (A Light In The Black / Tarot Woman / Stargazer / Kill The King)>
	<When A Blind Man Cries>
	<Remember Tomorrow>
	<Helpless>
	<Fade To Black>
	<Jump In The Fire>
	<Frantic>
	<St. Anger>
	<Some Kind Of Monster>
	<Dirty Window>
	<Invisible Kid>
	<My World>
	<Shoot Me Again>
	<Sweet Amber>
	<The Unnamed Feeling>
	<Purify>
	<All Within My Hands>
	<The Ecstasy Of Gold>
	<The Call Of Ktulu>
	<Master Of Puppets>
	<Of Wolf And Man>
	<The Thing That Should Not Be>
	<Fuel>
	<The Memory Remains>
	<No Leaf Clover>
	<Hero Of The Day>
	<Devil's Dance>
	<Bleeding Me>
	<Nothing Else Matters>
	<Until It Sleeps>
	<For Whom The Bell Tolls>
	<- Human>
	<Wherever I May Roam>
	<Outlaw Torn>
	<Sad But True>
	<One>
	<Enter Sandman>
	<Battery>
	<The Unforgiven II>
	<Better Than You>
	<Slither>
	<Carpe Diem Baby>
	<Bad Seed>
	<Where The Wild Things Are>
	<Prince Charming>
	<Low Man's Lyric>
	<Attitude>
	<Fixxxer>
	<Holier Than Thou>
	<The Unforgiven>
	<Don't Tread On Me>
	<Through The Never>
	<The God That Failed>
	<My Friend Of Misery>
	<The Struggle Within>
	<Blackened>
	<...And Justice for All>
	<Eye of the Beholder>
	<The Shortest Straw>
	<Harvester of Sorrow>
	<The Frayed Ends of Sanity>
	<To Live is to Die>
	<Dyers Eve>
	<Fight Fire With Fire>
	<Ride The Lightning>
	<Trapped Under Ice>
	<Escape>
	<Creeping Death>
	<Hit The Lights>
	<The Four Horsemen>
	<Motorbreath>
	<(Anesthesia) Pulling Teeth>
	<Whiplash>
	<Phantom Lord>
	<No Remorse>
	<Seek & Destroy>
	<Metal Militia>
	<Welcome Home (Sanitarium)>
	<Disposable Heroes>
	<Leper Messiah>
	<Orion>
	<Damage, Inc.>
	<That Was Just Your Life>
	<The End Of The Line>
	<Broken, Beat & Scarred>
	<The Day That Never Comes>
	<All Nightmare Long>
	<Cyanide>
	<The Judas Kiss>
	<Suicide & Redemption>
	<My Apocalypse>
	<Ain't My Bitch>
	<2 X 4>
	<The House That Jack Built>
	<King Nothing>
	<Cure>
	<Poor Twisted Me>
	<Wasting My Hate>
	<Mama Said>
	<Thorn Within>
	<Ronnie>
	<The Outlaw Torn>
	<Am I Evil?>
	<The Unforgiven (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Disposable Heroes (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<When A Blind Man Cries (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Please Don't Judas Me (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Turn The Page (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Bleeding Me (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Veteran of the Psychic Wars (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Nothing Else Matters (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<All Within My Hands (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Enter Sandman (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<The Four Horsemen (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Hardwired (Live At The Masonic, San Francisco, CA - November 3rd, 2018)>
	<Blackened 2020>
	<All Within My Hands (Live)>


Led Zeppelin
	<All My Love (Remaster)>
	<Immigrant Song (2012 Remaster)>
	<Since I've Been Loving You (2012 Remaster)>
	<Stairway to Heaven (Remaster)>
	<What Is and What Should Never Be (Remaster)>
	<Dazed and Confused (2014 Remaster)>
	<Kashmir (2012 Remaster)>
	<Black Dog (2012 Remaster)>
	<Whole Lotta Love (Remaster)>
	<Good Times Bad Times (2014 Remaster)>
	<Going to California (2012 Remaster)>
	<Bron-Y-Aur Stomp (2012 Remaster)>
	<Rock and Roll (2012 Remaster)>
	<Trampled Under Foot (2012 Remaster)>
	<Houses of the Holy (1990 Remaster)>
	<The Ocean (2012 Remaster)>
	<Heartbreaker (Remaster)>
	<When the Levee Breaks (2012 Remaster)>
	<Moby Dick (Remaster)>
	<Tangerine (2012 Remaster)>


Tenacious D
	<Beelzeboss (The Final Showdown)>
	<Kickapoo>
	<Tribute>
	<The Metal>
	<History>


Accept
	<Metal Heart>
	<Balls to the Wall>


Scorpions
	<Still Loving You>
	<Holiday>
	<Wind Of Change>


Anthrax
	<I'm The Man (Def Uncensored Version)>
	<Among The Living>


alt-J
	<Left Hand Free>
	<(Guitar)>
	<Tessellate>
	<Something Good>
	<Intro>
	<(Piano)>
	<Breezeblocks>
	<Taro>
	<Matilda>
	<Dissolve Me>
	<Ms>
	<(The Ripe & Ruin)>
	<Hand-Made (Hidden Track)>
	<Bloodflood>
	<Fitzpleasure>


Judas Priest
	<Rising From Ruins>
	<Painkiller>
	<Breaking the Law>
	<Electric Eye>


David Bowie
	<Ziggy Stardust (2012 Remastered Version)>
	<Star (2012 Remastered Version)>
	<Lady Stardust (2012 Remastered Version)>
	<Rock 'N' Roll Suicide (2012 Remastered Version)>
	<It Ain't Easy (2012 Remastered Version)>
	<Suffragette City (2012 Remastered Version)>
	<Space Oddity (2015 Remaster)>
	<Soul Love (2012 Remastered Version)>
	<Five Years (2012 Remastered Version)>
	<Moonage Daydream (2012 Remastered Version)>
	<Starman (2012 Remastered Version)>
	<Hang On To Yourself (2012 Remastered Version)>
	<Fame (1999 Remaster)>


Urge Overkill
	<Girl, You'll Be a Woman Soon>


Nirvana
	<Come As You Are>
	<Smells Like Teen Spirit>
	<Lithium>
	<Polly>
	<Heart-Shaped Box>
	<The Man Who Sold The World>
	<Dumb>
	<In Bloom>
	<Something In The Way>
	<Drain You>


Billy Joel
	<Uptown Girl>
	<We Didn't Start the Fire>
	<Movin' Out (Anthony's Song)>
	<Piano Man (Radio Edit)>


White Stripes
	<Seven Nation Army>


My Chemical Romance
	<Welcome to the Black Parade>


Nick Murphy
	<No Diggity>
	<Gold>


AC/DC
	<Back In Black>
	<Highway To Hell>


Guns N' Roses
	<Sweet Child O' Mine>
	<Knockin' On Heaven's Door>


The Charlie Daniels Band
	<The Devil Went Down to Georgia>


Megadeth
	<Symphony Of Destruction>
	<Peace Sells>
	<In My Darkest Hour (Remastered 2004)>
	<A Tout Le Monde (Remastered 2004)>
	<Sweating Bullets>
	<Holy Wars...The Punishment Due (Remastered 2004)>
	<Rust In Peace...Polaris (Remastered 2004)>
	<Tornado Of Souls (Remastered 2004)>
	<Hangar 18 (Remastered 2004)>
	<Take No Prisoners (Remastered 2004)>
	<Five Magics (Remastered 2004)>
	<Poison Was The Cure>
	<Lucretia>
	<Dawn Patrol>
	<My Creation>


Kasabian
	<La Fee Verte>


Leo
	<Feel Good Inc. (Metal Cover)>


R.E.M.
	<Losing My Religion>
	<Everybody Hurts>
	<Shiny Happy People>
	<It's The End Of The World As We Know It (And I Feel Fine)>


Gary Moore
	<Still Got The Blues>


The Clash
	<Should I Stay or Should I Go (Remastered)>


Alice Cooper
	<Poison>


Eminem
	<Stan>
	<Mockingbird>
	<The Real Slim Shady>
	<Rap God>
	<My Name Is>


Bon Jovi
	<It's My Life>
	<Bad Medicine>
	<You Give Love A Bad Name>


The Rolling Stones
	<(I Can't Get No) Satisfaction>
	<Paint It, Black>
	<Beast Of Burden>
	<Sympathy For The Devil>


The Cure
	<Friday I'm in Love>
	<The Lovecats>


Depeche Mode
	<Behind the Wheel>
	<Personal Jesus (Original Single Version)>
	<Dream On (2007 Remastered Version)>


Queen
	<We Will Rock You>
	<We Are The Champions>
	<I Want It All>
	<Don't Stop Me Now>
	<Bicycle Race>
	<Fat Bottomed Girls>
	<Killer Queen>
	<Another One Bites The Dust>
	<Bohemian Rhapsody>
	<The Show Must Go On>
	<Under Pressure>
	<Somebody To Love>
	<Good Old-Fashioned Lover Boy>
	<Crazy Little Thing Called Love>
	<I Want To Break Free>


Eagles
	<Hotel California (2013 Remaster)>
	<Victim of Love (2013 Remaster)>


Simon & Garfunkel
	<The Sound of Silence>


Panic! At the Disco
	<I Write Sins Not Tragedies>
	<Death of a Bachelor>


Radiohead
	<Creep>
	<Street Spirit (Fade Out)>


George Michael
	<They Won't Go When I Go>


Arctic Monkeys
	<Arabella>
	<Do I Wanna Know?>
	<I Want It All>
	<Mad Sounds>
	<R U Mine?>
	<Snap Out Of It>
	<Knee Socks>
	<One For The Road>
	<Pretty Visitors>
	<Crying Lightning>
	<The Jeweller's Hands>
	<Fireside>
	<No. 1 Party Anthem>
	<I Wanna Be Yours>
	<Why'd You Only Call Me When You're High?>
	<Mardy Bum>
	<The View From The Afternoon>
	<Fluorescent Adolescent>


Sting
	<Shape Of My Heart>
	<Englishman In New York>
	<Moon Over Bourbon Street>
	<Russians>


Red Hot Chili Peppers
	<Otherside>
	<Californication>
	<Dani California>
	<Purple Stain>
	<Scar Tissue>
	<Around the World>
	<Road Trippin'>
	<Storm in a Teacup>
	<Slow Cheetah>
	<Snow (Hey Oh)>
	<Death of a Martian>
	<21st Century>
	<Make You Feel Better>
	<Hump de Bump>
	<Can't Stop>
	<Nobody Weird Like Me>
	<Under the Bridge>
	<Wet Sand>
	<Torture Me>
	<Hard to Concentrate>
	<The Hunter>
	<Give It Away>
	<Suck My Kiss>
	<By the Way>
	<Higher Ground>


Gnarls Barkley
	<Crazy>


Tanita Tikaram
	<Twist in My Sobriety>


Aerosmith
	<Dream On>
	<Walk This Way>


Beck
	<Loser>
	<O Maria>


Michael Jackson
	<Billie Jean>
	<Bad (2012 Remaster)>
	<Smooth Criminal (2012 Remaster)>
	<Beat It (Single Version)>
	<Thriller>
	<Dirty Diana (2012 Remaster)>
	<Jam>


Dire Straits
	<Brothers In Arms>
	<Sultans Of Swing>
	<Money For Nothing>
	<Setting Me Up>
	<Telegraph Road>
	<Private Investigations>
	<Industrial Disease>
	<Love Over Gold>
	<It Never Rains>


Dream Theater
	<Pull Me Under>
	<Metropolis - Part I: "The Miracle and the Sleeper">
	<Fall into the Light>
	<Paralyzed>
	<Under a Glass Moon>


DragonForce
	<Through The Fire And Flames>


Twisted Sister
	<I Wanna Rock>


Bob Dylan
	<Hurricane>


Fleetwood Mac
	<The Chain (2004 Remaster)>


The Animals
	<House of the Rising Sun>
	<Don't Let Me Be Misunderstood>


The Doors
	<Riders on the Storm>
	<People Are Strange>
	<Light My Fire>
	<Roadhouse Blues>


Disturbed
	<Down with the Sickness>


Tears for Fears
	<Mad World>


Oasis
	<Live Forever>


Blur
	<Parklife>
	<Song 2>


Bruce Springsteen
	<Streets of Philadelphia (Single Edit)>


The Cat Empire
	<The Lost Song>
	<Protons, Neutrons, Electrons>
	<All That Talking>


The White Stripes
	<Apple Blossom>


Derek & The Dominos
	<Layla>


Electric Light Orchestra
	<Mr. Blue Sky>
	<Ticket to the Moon>
	<21st Century Man>


Creedence Clearwater Revival
	<Fortunate Son>


Pantera
	<Walk>
	<Domination>
	<Cowboys from Hell>


Eric Clapton
	<Cocaine>


Jimi Hendrix
	<Little Wing>
	<All Along the Watchtower>
	<Purple Haze>
	<Hey Joe>


Tom Petty and the Heartbreakers
	<Mary Jane's Last Dance>


Avenged Sevenfold
	<A Little Piece of Heaven>
	<Nightmare>
	<Exist>


Black Sabbath
	<War Pigs>
	<Paranoid>
	<Iron Man>
	<The Mob Rules (2009 Remaster)>


Mason Williams
	<Classical Gas>


Eric Johnson
	<Cliffs Of Dover (Instrumental)>


Van Halen
	<Eruption>


Ozzy Osbourne
	<Crazy Train (2002 Version)>


Muse
	<Hysteria>
	<Unintended>


The Offspring
	<Come Out and Play>


Cream
	<Sunshine Of Your Love>
	<White Room>


Stealers Wheel
	<Stuck In The Middle With You (Remastered)>


Jerry Lee Lewis
	<Great Balls Of Fire>


Talking Heads
	<Psycho Killer (Acoustic Version)>


The Beatles
	<Sgt. Pepper's Lonely Hearts Club Band (Remix)>
	<With A Little Help From My Friends (Remix)>
	<Lucy In The Sky With Diamonds (Remix)>
	<Getting Better (Remix)>
	<Fixing A Hole (Remix)>
	<She's Leaving Home (Remix)>
	<Being For The Benefit Of Mr. Kite! (Remix)>
	<Within You Without You (Remix)>
	<When I'm Sixty-Four (Remix)>
	<Lovely Rita (Remix)>
	<Good Morning Good Morning (Remix)>
	<A Day In The Life (Remix)>
	<Happiness Is A Warm Gun>


Queens of the Stone Age
	<No One Knows>


Pearl Jam
	<Even Flow>


U2
	<Sunday Bloody Sunday>


Iggy Pop
	<The Passenger>


Toto
	<Hold the Line>


Mot�rhead
	<Ace Of Spades>
	<Bomber>


Yes
	<Owner of a Lonely Heart>
	<Roundabout (2003 Remaster)>
	<A Venture (2003 Remaster)>
	<The Gates of Delirium (2003 Remaster)>
	<The Revealing Science of God (Dance of the Dawn) (2003 Remaster)>
	<Siberian Khatru (2003 Remaster)>
	<Starship Trooper: Life Seeker (Single Version)>
	<America (2003 Remaster)>
	<Heart of the Sunrise (2003 Remaster)>
	<Mood for a Day (2003 Remaster)>
	<Close to the Edge (i. The Solid Time of Change, ii. Total Mass Retain, iii. I Get up I Get Down, iv. Seasons of Man) (2003 Remaster)>


Genesis
	<Dancing With The Moonlit Knight>
	<I Know What I Like (In Your Wardrobe)>
	<Firth Of Fifth>
	<More Fool Me>
	<The Battle Of Epping Forest>
	<After The Ordeal>
	<The Cinema Show>
	<Aisle Of Plenty>
	<Supper's Ready>
	<That's All>
	<Robbery, Assault And Battery>
	<Get 'Em Out By Friday>
	<The Musical Box>
	<Horizons>
	<Blood On The Rooftops>
	<Eleventh Earl Of Mar>
	<One For The Vine>
	<Your Own Special Way>
	<Wot Gorilla?>
	<All In A Mouse's Night>
	<Unquiet Slumbers For The Sleepers>
	<In That Quiet Earth>
	<Afterglow>
	<Carpet Crawlers>


Steve Vai
	<Valhalla / Baroque n Roll / Overture / From a Thousand Cuts / Arpeggios from Hell / Far Beyond the Sun (Live)>


Joan Jett and the Blackhearts
	<I Love Rock 'N Roll>


King Crimson
	<Cirkus (including Entry of the Chameleons)>
	<Indoor Games>
	<Happy Family>
	<Lady of the Dancing Water>
	<Lizard (Prince Rupert Awakes/Bolero/The Battle of Glass Tears/Big Top)>
	<Bolero (remix from Frame By Frame)>
	<Larks' Tongues In Aspic (Part I)>
	<Book Of Saturday>
	<Exiles>
	<Easy Money>
	<The Talking Drum>
	<Larks' Tongues In Aspic (Part II)>
	<Recording Session Extract (Part 1) [Bonus Track]>
	<Recording Session Extract (Part 2) [Bonus Track]>
	<21st Century Schizoid Man (Including "Mirrors")>
	<I Talk To The Wind>
	<Epitaph (Including "March for No Reason" and "Tomorrow and Tomorrow")>
	<Moonchild (Including "The Dream" and "The Illusion")>
	<The Court Of The Crimson King (Including "The Return of the Fire Witch" and "The Dance of the Puppets")>
	<21st Century Schizoid Man [Bonus Track] (Radio Version)>
	<I Talk To The Wind [Bonus Track] (Duo Version)>
	<A Man A City [Bonus Track] (Live At the Fillmore West)>
	<The Great Deceiver>
	<Lament>
	<We'll Let You Know>
	<The Night Watch>
	<Trio>
	<The Mincer>
	<Starless And Bible Black>
	<Fracture>
	<The Law Of Maximum Distress (including "The Mincer")>
	<Dr. Diamond (Live 23 june, 1973)>
	<Guts On My Side (Live audience recording 19 March, 1973)>
	<Peace - A Beginning>
	<Pictures Of A City>
	<Cadence and Cascade>
	<In The Wake Of Poseidon>
	<Peace - A Theme>
	<Cat Food>
	<The Devils Triangle>
	<Peace - An End>
	<Cat Food (single version)>
	<Groon (single B-side)>
	<Cadence And Cascade (Greg Lake guide vocal)>
	<Three of a Perfect Pair>
	<Model Man>
	<Sleepless>
	<Man With An Open Heart>
	<Nuages (That Which Passes, Passes Like Clouds)>
	<Industry>
	<Dig Me>
	<No Warning>
	<Larks' Tongues In Aspic, Pt. III>
	<The King Crimson Barber Shop>
	<Industrial Zone A>
	<Industrial Zone B>
	<Sleepless (Tony Levin Mix)>
	<Sleepless (Bob Clearmountain Mix)>
	<Sleepless (Francois Kevorkian dance mix)>
	<Formentera Lady>
	<Sailor's Tale>
	<The Letters>
	<Ladies of the Road>
	<Prelude - Song Of The Gulls>
	<Islands>
	<Studio Sessions>
	<Ladies of the Road (Robert Fripp/David Singleton Remix)>
	<Red>
	<Fallen Angel>
	<One More Red Nightmare>
	<Providence>
	<Starless>
	<Elephant Talk>
	<Frame By Frame>
	<Matte Kudasai>
	<Indiscipline>
	<Thela Hun Ginjeet>
	<The Sheltering Sky>
	<Discipline>
	<Matte Kudasai (alternate version)>
	<The Terrifying Tale of Thela Hun Ginjeet>
	<21st Century Schizoid Man(Live)>
	<I Talk To The Wind (From: The Young Person's Guide To King Crimson 1976)>
	<The Court of the Crimson King(Live)>


Alice in Chains
	<Them Bones>
	<Dam That River>
	<Rain When I Die>
	<Down In A Hole>
	<Sickman>
	<Rooster>
	<Junkhead>
	<Dirt>
	<God Smack>
	<Untitled>
	<Hate To Feel>
	<Angry Chair>
	<Would?>
	<We Die Young>
	<Man in the Box>
	<Sea Of Sorrow>
	<Love, Hate, Love (Album Version)>
	<Am I Inside>
	<Brother (Album Version)>
	<Got Me Wrong>
	<Right Turn>
	<No Excuses>
	<I Stay Away>
	<What the Hell Have I (Remix)>
	<A Little Bitter (Remix)>
	<Grind>
	<Heaven Beside You>
	<Again>
	<Over Now (Unplugged)>
	<Nutshell (Unplugged)>
	<Get Born Again>
	<Died>


Mike Oldfield
	<Nuclear>


Green Day
	<Hitchin' a Ride>


TOOL
	<Vicarious>
	<Jambi>
	<Wings For Marie (Pt 1)>
	<10,000 Days (Wings Pt 2)>
	<The Pot>
	<Lipan Conjuring>
	<Lost Keys (Blame Hofman)>
	<Rosetta Stoned>
	<Intension>
	<Right In Two>
	<Viginti Tres>
	<Intolerance>
	<Prison Sex>
	<Sober>
	<Bottom>
	<Crawl Away>
	<Swamp Song>
	<Undertow>
	<4�>
	<Flood>
	<Disgustipated>
	<Stinkfist>
	<Eulogy>
	<H.>
	<Useful Idiot>
	<Forty Six & 2>
	<Message To Harry Manback>
	<Hooker With A Penis>
	<Intermission>
	<Jimmy>
	<Die Eier von Satan>
	<Pushit>
	<Cesaro Summability>
	<�nema>
	<(-) Ions>
	<Third Eye>
	<The Grudge>
	<Eon Blue Apocalypse>
	<The Patient>
	<Mantra>
	<Schism>
	<Parabol>
	<Parabola>
	<Ticks & Leeches>
	<Lateralus>
	<Disposition>
	<Reflection>
	<Triad>
	<Faaip De Oiad>
	<Sweat>
	<Hush>
	<Part Of Me>
	<Cold And Ugly (Live)>
	<Jerk-Off (Live)>
	<Opiate>


Blue �yster Cult
	<(Don't Fear) The Reaper>


Madness
	<The Prince>
	<One Step Beyond>
	<My Girl>
	<Bed and Breakfast Man>
	<Night Boat To Cairo>
	<Madness>
	<Baggy Trousers>
	<Embarrassment>
	<The Return of the Los Palmas 7>
	<Grey Day>
	<Shut Up>
	<It Must Be Love>
	<Cardiac Arrest>
	<House of Fun>
	<Driving In My Car>
	<Our House>
	<Tomorrow's (Just Another Day)>
	<Wings of a Dove>
	<The Sun and the Rain>
	<Michael Caine>
	<One Better Day>
	<Uncle Sam>
	<Yesterday's Men>
	<(Waiting For The) Ghost Train>
	<Sarah's Song (From "Our House" Musical)>
	<Lovestruck>
	<Johnny the Horse>
	<Drip Fed Fred (feat. Ian Dury)>
	<Simple Equation (From "Our House" Musical)>
	<Girl Why Don't Ya>
	<NW5>
	<Dust Devil>
	<Forever Young>
	<Sugar And Spice>
	<My Girl 2>
	<Never Knew Your Name>
	<How Can I Tell You>
	<Misery>
	<La Luna>
	<Mr. Apples>
	<Can't Touch Us Now>
	<Another Version of Me>


Yngwie Malmsteen
	<Trilogy Suite Op: 5>


Rush
	<La Villa Strangiato>
	<Tom Sawyer>
	<Cygnus X-1 Book II: Hemispheres>
	<YYZ (Album Version)>
	<Circumstances>
	<Working Man>
	<Nobody's Hero (2004 Remaster)>
	<The Trees>
	<Natural Science>


Jethro Tull
	<Aqualung>
	<Bour�e (Steven Wilson Remix)>
	<A New Day Yesterday (2001 Remaster)>
	<Jeffrey Goes to Leicester Square (2001 Remaster)>
	<Bour�e (2001 Remaster)>
	<Back to the Family (2001 Remaster)>
	<Look into the Sun (2001 Remaster)>
	<Nothing Is Easy (2001 Remaster)>
	<Fat Man (2001 Remaster)>
	<We Used to Know (2001 Remaster)>
	<Reasons for Waiting (2001 Remaster)>
	<For a Thousand Mothers (2001 Remaster)>
	<Living in the Past (2001 Remaster)>
	<Driving Song (2001 Remaster)>
	<Sweet Dream (2001 Remaster)>
	<17 (2001 Remaster)>
	<Too Old to Rock 'n' Roll: Too Young to Die! (Steven Wilson Stereo Remix)>
	<Moths (2003 Remaster)>
	<Aqualung (Steven Wilson Stereo Remix)>
	<Cross-Eyed Mary (Steven Wilson Stereo Remix)>
	<Cheap Day Return (Steven Wilson Stereo Remix)>
	<Mother Goose (Steven Wilson Stereo Remix)>
	<Wond'ring Aloud (Steven Wilson Stereo Remix)>
	<Up to Me (Steven Wilson Stereo Remix)>
	<My God (Steven Wilson Stereo Remix)>
	<Hymn 43 (Steven Wilson Stereo Remix)>
	<Slipstream (Steven Wilson Stereo Remix)>
	<Locomotive Breath (Steven Wilson Stereo Remix)>
	<Wind-Up (Steven Wilson Stereo Remix)>
	<Thick as a Brick (Pt. I) (1997 Remaster)>
	<Thick as a Brick (Pt. II) (1997 Remaster)>
	<Heavy Horses (2003 Remaster)>
	<Budapest (2005 Remaster)>


Chuck Berry
	<Johnny Be Goode>


Rage Against the Machine
	<Killing In The Name>


Elvis Presley
	<Jailhouse Rock>


Slade
	<Cum on Feel the Noize>


Neil Young
	<Rockin' in the Free World>


ZZ Top
	<La Grange (2005 Remaster)>


The Kinks
	<Sunny Afternoon>


Caravan
	<Nine Feet Underground>


Dick Dale
	<Misirlou (From "Pulp Fiction")>


Hawkwind
	<Assault and Battery / The Golden Void>
	<The Psychedelic Warlords (Disappear in Smoke) (1996 - Remaster)>


Babe Ruth
	<Black Dog>
	<The Mexican>


Steven Wilson
	<Drive Home>
	<Luminol>
	<The Holy Drinker>


Dio
	<Holy Diver>
	<The Last In Line>
	<Rainbow In The Dark>


Iron Butterfly
	<In-A-Gadda-Da-Vida (2006 Remaster Full-Length)>


Family
	<Drowned in Wine>


The Move
	<Blackberry Way>


The New Vaudeville Band
	<I Was Lord Kitchner's Valet>


Eels
	<Trouble with Dreams>
	<Novocaine For The Soul>
	<That Look You Give That Guy>


Crowded House
	<Nails In My Feet>


The Record Low
	<The Bottom>


Herman's Hermits
	<No Milk Today>


Elliott Smith
	<Waltz #2 (XO)>
	<Tomorrow Tomorrow>


The Apples in Stereo
	<Benefits of Lying (With Your Friend)>


Primus
	<Seas Of Cheese>
	<Here Come The Bastards>
	<Sgt. Baker>
	<American Life>
	<Jerry Was A Race Car Driver>
	<Eleven>
	<Is It Luck?>
	<Grandad's Little Ditty>
	<Tommy The Cat>
	<Sathington Waltz>
	<Those Damned Blue-Collar Tweekers>
	<Fish On (Fisherman Chronicles, Chapter II)>
	<Los Bastardos>
	<DMV>
	<John The Fisherman (Album Version)>
	<Wynona's Big Brown Beaver>
	<Have A Cigar>
	<Eclectic Electric>
	<Lacquer Head>
	<Mr. Krinkle>


Keith Emerson
	<The Only Way (Hymn) (2012 Remastered Version)>
	<Karn Evil 9 1st Impression, Pt. 2 (2014 Remastered Version)>
	<Take a Pebble (2012 Remastered Version)>


Camel
	<Lady Fantasy>
	<Echoes>
	<Supertwister>


Lamb of God
	<Laid to Rest>


Wishbone Ash
	<The King Will Come>
	<Blind Eye>
	<Warrior>


Focus
	<House Of The King>


Traffic
	<Glad (Remastered 2010)>


The Alan Parsons Project
	<Silence and I>


Porcupine Tree
	<The Sound Of Muzak>


Van der Graaf Generator
	<Cat's Eye/Yellow Fever (Running)>
	<A Plague Of Lighthouse Keepers (Medley)>


Blackfoot
	<Highway Song>


King Gizzard And The Lizard Wizard
	<Nuclear Fusion>


Toadies
	<Possum Kingdom>


Chris Cornell
	<One (Live At Beacon Theatre/2015)>


Chet Atkins
	<Windy and Warm>


Steppenwolf
	<Born To Be Wild>


Dust
	<From A Dry Camel>


Chrome
	<Moonchild>


The Black Keys
	<Lonely Boy>


Kansas
	<Carry on Wayward Son>


Cage The Elephant
	<Ain't No Rest for the Wicked>


The Blasters
	<Dark Night>


Status Quo
	<Roll Over Lay Down>
	<In The Army Now>


Montrose
	<Rock Candy (Remastered LP Verison)>


Monty Python
	<Always Look On The Bright Side Of Life>


Stevie Wonder
	<Master Blaster (Jammin')>
	<Higher Ground>


Ray Charles
	<Hit the Road Jack>


The Commitments
	<Treat Her Right>


Hawkwind Zoo
	<Cymbaline (1996 Remaster)>


Victor Wooten
	<Classical Thump>


David Gilmour
	<Dancing Right In Front of Me>


Rodrigo y Gabriela
	<Hanuman>


Al Di Meola
	<Mediterranean Sundance / Rio Ancho (Live at Warfield Theatre, San Francisco, CA - December 1980)>


Coolio
	<Gangsta's Paradise>


