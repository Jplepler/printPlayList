import requests

BASE_URL = "https://api.deezer.com/"


def getID():
    """
    This function gets the id of the playlist / album
    :return: the id of the playlist / album (int)
    """
    modified_url = BASE_URL + 'search/' + 'playlist' + '?q=' + input(
        "Enter name of playlist and make sure he appears first under the playlist category: ") + '/'

    print(modified_url)
    r = requests.get(url=modified_url)

    data = r.json()

    return data["data"][0]["id"]  # Get ID of playlist/album


def main():
    counter = 0
    playlist_id = getID()
    r = requests.get(url=BASE_URL + 'playlist/' + str(playlist_id) + '/tracks', stream=True)

    songs_dict = {}

    data = r.json()

    # get artists
    for i in data['data']:
        if i["artist"]["name"] not in songs_dict.keys():
            songs_dict[i["artist"]["name"]] = []

    # get songs
    for i in data['data']:
        songs_dict[i["artist"]["name"]].append(i["title"])

    try:
        while True:
            data = r.json()

            # get artists
            for i in data['data']:
                if i["artist"]["name"] not in songs_dict.keys():
                    songs_dict[i["artist"]["name"]] = []

            # get songs
            for i in data['data']:
                songs_dict[i["artist"]["name"]].append(i["title"])

            r = requests.get(url=data['next'], stream=True)

    except Exception as e:
        print(songs_dict)

    # before printing in file, remove duplicates
    for i in songs_dict.keys():
        songs_dict[i] = list(dict.fromkeys(songs_dict[i]))

    with open("output.txt", "w") as f:
        for i in songs_dict.keys():
            f.write(i + '\n')

            for k in songs_dict[i]:
                f.write('\t' + '<' + k + '>' + '\n')
            f.write('\n\n')


if __name__ == '__main__':
    main()
